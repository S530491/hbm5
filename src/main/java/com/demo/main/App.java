package com.demo.main;


import org.hibernate.Session;
import org.hibernate.Transaction;

import com.demo.util.HibernateUtil;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session session=null;
try {
		session=HibernateUtil.getSessionFactory().openSession();
Transaction t=session.beginTransaction();
	String sql="select version()";
	String result=session.createNativeQuery(sql).getSingleResult().toString();
	System.out.println(result);
	t.commit();
	session.close();
	
	}catch(Exception e) {
		e.printStackTrace();
	}
HibernateUtil.shutdown();

}
}
