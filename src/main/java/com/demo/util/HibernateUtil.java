package com.demo.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {
private static StandardServiceRegistry registry;
public static SessionFactory factory;
//only when object is null then it will initialize
public static SessionFactory getSessionFactory(){
	if(factory==null) {
		try {
		registry=new StandardServiceRegistryBuilder().configure().build();
	MetadataSources sources=new MetadataSources(registry)
;
	Metadata metadata=sources.getMetadataBuilder().build();
	factory=metadata.getSessionFactoryBuilder().build();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	return factory;
}
public static void shutdown() {
	if(registry!=null) {
		StandardServiceRegistryBuilder.destroy(registry);
	}
}
}
